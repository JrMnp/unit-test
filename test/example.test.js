// test/example.test.js

const expect = require('chai').expect;
const should = require('chai').should();
const { assert } = require('chai');
const { arrayGen } = require('../src/mylib');
const mylib = require('../src/mylib');

describe('Unit testing mylib.js', () => {

    let myvar = undefined

    before(() => {
        myvar = 1; // setup before testing
        console.log('Before testing let´s have a countdown...');
        console.log('3');
        console.log('2');
        console.log('1');
    })

    it('Should return 2 when using sum function with a=1, b=1', () => {
        const result = mylib.sum(1,1); // 1+1
        expect(result).to.equal(2); // result expected to equal 2
    })

    it('Assert foo is not bar', () => {
        assert('foo' !== 'bar') // true
    })

    it('Myvar should exist', () => {
        should.exist(myvar);
    })

    it('parametrized way of unit testing', () => {
        const result = mylib.sum(myvar, myvar);
        expect(result).to.equal(myvar+myvar);
    })

    it('Assert right is not wrong', () => {
        assert('right' !== 'wrong') // true
    })

    after(() => {
        console.log('After testing...AAAND it´s DONE');
    })

})